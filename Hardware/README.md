# rotorCore

## About
rotorCore is part of WUST Ground Station project. This embedded motherboard is dedicated to:
- Operate rotor hardware (2 stepper motors)
- Operate main antenna of ground station (via LoRa module)

## Main features
- Two ESP32E modules:
    - One to control stepper motors (via stepper motor controllers)
    - Second for proxy between Raspberry (main computer of entire device; not included in this project) and LoRa module
- LoRa module (model Ra-02 SX1278)

## Other features
- All main components are detachable THT modules. This includes:
    - 2 DRV8234 stepper motor drivers
    - 2 ESP-32E modules
    - LoRa module
- Both ESP32E are connected with each other via UART, to achieve software redundancy.
- 3 limit swiches to align starting position and  movement limits of stepper motors (handled by motor ESP). 
- 2 power voltage levels:
    - 3,3V for uC and other logical units. Provided by LF33 linear stabiliser.
    - ~7,8V to power stepper motors. Provided by LM2596 adjustable step-down buck converter module (hard-soldered).

## Stepper motors
Both steppers are identical JK42HS60. One of them provides horizontal traverse (360deg), while second one provides adjustable elevation (-13deg to 23deg)*.
*where 0deg means horizontal position of antenna frame (antenna aiming vertically upwards). 

Elevation/depression limitations are based on rotor construction and cannot be exceed because of risk of colission between antenna and rotor hull.  Both limits will be guarded by limit swiches (similarly to movement limitation system of FDM 3D printer).

Horizontal stepper cannot make more than one rotation, because it will stretch and damage power/data cables. To prevent that, there are one limit swich to allign starting position of rotor horizontal joint (also similarly to movement limitation system of FDM 3D printer). 
