#include <Arduino.h>

#include <WiFi.h>
#include <PubSubClient.h>
#include <AccelStepper.h>

// Define pin assignments
#define LIM_ELEV_1 13
#define DIR_ELEV 18
#define STEP_ELEV 19
#define NEGSLEEP_ELEV 21
#define M1_ELEV 22
#define M0_ELEV 23

#define NEGSLEEP_HOR 25
#define M1_HOR 26
#define M0_HOR 27
#define DIR_HOR 32
#define STEP_HOR 33
#define LIM_HOR 34
#define LIM_ELEV_2 35

// WiFi and MQTT settings
const char* ssid = "JuliuszWiFi";
const char* password = "DzikiDzik";
const char* mqtt_server = "kalenica.local";
const char* mqtt_topic_elev = "topic/elev";
const char* mqtt_topic_hor = "topic/hor";

// Initialize WiFi and MQTT client
WiFiClient espClient;
PubSubClient client(espClient);

// Initialize the stepper motors
AccelStepper stepperELEV(AccelStepper::DRIVER, STEP_ELEV, DIR_ELEV);
AccelStepper stepperHOR(AccelStepper::DRIVER, STEP_HOR, DIR_HOR);

void moveElevatorTo(int targetPosition) {
  if (digitalRead(LIM_ELEV_1) == LOW && targetPosition < stepperELEV.currentPosition()) {
    Serial.println("ELEV limit switch 1 triggered, can't move further down.");
    return;
  } else if (digitalRead(LIM_ELEV_2) == LOW && targetPosition > stepperELEV.currentPosition()) {
    Serial.println("ELEV limit switch 2 triggered, can't move further up.");
    return;
  }

  stepperELEV.moveTo(targetPosition);
}

void moveHorizontalTo(int targetPosition) {
  if (digitalRead(LIM_HOR) == LOW && targetPosition < stepperHOR.currentPosition()) {
    Serial.println("HOR limit switch triggered, can't move further left.");
    return;
  }

  stepperHOR.moveTo(targetPosition);
}

void moveToHome() {
  // Move HOR motor to home position
  stepperHOR.setSpeed(-50); // Move towards the limit switch
  while (digitalRead(LIM_HOR) != LOW) {
    stepperHOR.runSpeed();
  }
  stepperHOR.stop();
  Serial.println("HOR motor homed.");

  // Move ELEV motor to home position
  stepperELEV.setSpeed(-50); // Move towards the limit switch
  while (digitalRead(LIM_ELEV_1) != LOW) {
    stepperELEV.runSpeed();
  }
  stepperELEV.stop();
  Serial.println("ELEV motor homed.");
}

void setup_wifi() {
  delay(10);
  Serial.println();
  Serial.print("Connecting to ");
  Serial.println(ssid);

  WiFi.begin(ssid, password);

  while (WiFi.status() != WL_CONNECTED) {
    delay(500);
    Serial.print(".");
  }

  Serial.println("");
  Serial.println("WiFi connected");
  Serial.println("IP address: ");
  Serial.println(WiFi.localIP());
}

void reconnect() {
  // Loop until we're reconnected
  while (!client.connected()) {
    Serial.print("Attempting MQTT connection...");
    // Attempt to connect
    if (client.connect("ESP32Client")) {
      Serial.println("connected");
      // Subscribe to topics
      client.subscribe(mqtt_topic_elev);
      client.subscribe(mqtt_topic_hor);
    } else {
      Serial.print("failed, rc=");
      Serial.print(client.state());
      Serial.println(" try again in 5 seconds");
      // Wait 5 seconds before retrying
      delay(5000);
    }
  }
}

void callback(char* topic, byte* payload, unsigned int length) {
  String message = "";
  for (unsigned int i = 0; i < length; i++) {
    message += (char)payload[i];
  }
  Serial.print("Message arrived [");
  Serial.print(topic);
  Serial.print("] ");
  Serial.println(message);

  if (String(topic) == mqtt_topic_elev) {
    int targetPosition = message.toInt();
    moveElevatorTo(targetPosition);
  } else if (String(topic) == mqtt_topic_hor) {
    int targetPosition = message.toInt();
    moveHorizontalTo(targetPosition);
  }
}

void setup() {
  // Set pin modes
  pinMode(LIM_ELEV_1, INPUT);
  pinMode(LIM_HOR, INPUT);
  pinMode(LIM_ELEV_2, INPUT);

  pinMode(NEGSLEEP_ELEV, OUTPUT);
  pinMode(M1_ELEV, OUTPUT);
  pinMode(M0_ELEV, OUTPUT);
  
  pinMode(NEGSLEEP_HOR, OUTPUT);
  pinMode(M1_HOR, OUTPUT);
  pinMode(M0_HOR, OUTPUT);

  // Wake up the motor drivers
  digitalWrite(NEGSLEEP_ELEV, LOW);
  digitalWrite(NEGSLEEP_HOR, LOW);

  // Configure microstepping (example: full step)
  digitalWrite(M1_ELEV, LOW);
  digitalWrite(M0_ELEV, LOW);
  
  digitalWrite(M1_HOR, LOW);
  digitalWrite(M0_HOR, LOW);

  // Set maximum speed and acceleration
  stepperELEV.setMaxSpeed(100);
  stepperELEV.setAcceleration(50);
  
  stepperHOR.setMaxSpeed(100);
  stepperHOR.setAcceleration(50);

  Serial.begin(115200);

  // // Initialize WiFi and MQTT
  // setup_wifi();
  // client.setServer(mqtt_server, 1883);
  // client.setCallback(callback);

  // Move motors to home position
  moveToHome();
}

void loop() {
  // if (!client.connected()) {
  //   reconnect();
  // }
  // client.loop();

  // Check limit switches for ELEV motor
  if (digitalRead(LIM_ELEV_1) == LOW) {
    stepperELEV.stop();
    Serial.println("LIM_ELEV_1 triggered!");
  } else if (digitalRead(LIM_ELEV_2) == LOW) {
    stepperELEV.stop();
    Serial.println("LIM_ELEV_2 triggered!");
  } else {
      stepperELEV.run();
  }

  // Check limit switch for HOR motor
  if (digitalRead(LIM_HOR) == LOW) {
    stepperHOR.stop();
    Serial.println("LIM_HOR triggered!");
  } else {
    stepperHOR.run();
  }


  // moveElevatorTo(2137);
}

